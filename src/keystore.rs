use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use crate::*;

pub trait KeyStore {
    fn get_private_key(&self, ski: &[u8]) -> fcrypto::Result<Rc<PKey<Private>>>;

    fn get_public_key(&self, ski: &[u8]) -> fcrypto::Result<Rc<PKey<Public>>>;

    fn put_private_key(&mut self, ski: &[u8], key: PKey<Private>) -> fcrypto::Result<()>;

    fn put_public_key(&mut self, ski: &[u8], key: PKey<Public>) -> fcrypto::Result<()>;
}

pub struct FileKeyStore {
    entries: HashMap<Vec<u8>, PathBuf>,
}

impl FileKeyStore {
    pub fn new(path: &Path) -> fcrypto::Result<FileKeyStore> {
        let mut entries = HashMap::new();

        FileKeyStore::read_entries(&mut entries, path)?;

        let key_store = FileKeyStore { entries };

        return Ok(key_store);
    }

    pub fn new_multi(paths: Vec<&Path>) -> fcrypto::Result<FileKeyStore> {
        let mut entries = HashMap::new();

        for path in paths {
            FileKeyStore::read_entries(&mut entries, path)?;
        }

        let key_store = FileKeyStore { entries };

        return Ok(key_store);
    }

    fn read_entries(entries: &mut HashMap<Vec<u8>, PathBuf>, path: &Path) -> fcrypto::Result<()> {
        let key_store_dir = fs::read_dir(path)?;

        let paths = key_store_dir
            .map(|res| res.map(|entry| entry.path()))
            .collect::<io::Result<Vec<_>>>()?;

        for path in paths {
            let data = fs::read(&path)?;
            let ski = calculate_ski(&data)?;

            entries.insert(ski, path);
        }

        Ok(())
    }
}

impl KeyStore for FileKeyStore {
    fn get_private_key(&self, ski: &[u8]) -> fcrypto::Result<Rc<PKey<Private>>> {
        let key_path = self.entries.get(ski).ok_or("Key not found")?;
        let key_data = fs::read(&key_path)?;

        let key = PKey::private_key_from_pem(&key_data).map_err(wrap_error)?;

        return Ok(Rc::new(key));
    }

    fn get_public_key(&self, ski: &[u8]) -> fcrypto::Result<Rc<PKey<Public>>> {
        let key_path = self.entries.get(ski).ok_or("Key not found")?;
        let key_data = fs::read(key_path)?;

        let key = PKey::public_key_from_pem(&key_data).map_err(wrap_error)?;

        return Ok(Rc::new(key));
    }

    fn put_private_key(&mut self, ski: &[u8], key: PKey<Private>) -> fcrypto::Result<()> {
        unimplemented!();
    }

    fn put_public_key(&mut self, ski: &[u8], key: PKey<Public>) -> fcrypto::Result<()> {
        unimplemented!();
    }
}

pub struct InMemoryKeyStore {
    private_keys: HashMap<Vec<u8>, Rc<PKey<Private>>>,
    public_keys: HashMap<Vec<u8>, Rc<PKey<Public>>>,
}

impl InMemoryKeyStore {
    pub fn new() -> InMemoryKeyStore {
        let key_store = InMemoryKeyStore {
            private_keys: HashMap::new(),
            public_keys: HashMap::new(),
        };

        return key_store;
    }
}

impl KeyStore for InMemoryKeyStore {
    fn get_private_key(&self, ski: &[u8]) -> fcrypto::Result<Rc<PKey<Private>>> {
        let key = self.private_keys.get(ski).ok_or("Key not found")?;

        return Ok(key.clone());
    }

    fn get_public_key(&self, ski: &[u8]) -> fcrypto::Result<Rc<PKey<Public>>> {
        let key = self.public_keys.get(ski).ok_or("Key not found")?;

        return Ok(key.clone());
    }

    fn put_private_key(&mut self, ski: &[u8], key: PKey<Private>) -> fcrypto::Result<()> {
        self.private_keys.insert(ski.to_vec(), Rc::new(key));
        return Ok(());
    }

    fn put_public_key(&mut self, ski: &[u8], key: PKey<Public>) -> fcrypto::Result<()> {
        self.public_keys.insert(ski.to_vec(), Rc::new(key));
        return Ok(());
    }
}

impl Default for InMemoryKeyStore {
    fn default() -> Self {
        Self::new()
    }
}
