use std::env;
use std::fs;
use std::io;
use std::process;

use oper::calculate_ski;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("usage: print_ski <keystore>");
        process::exit(1);
    }

    let key_store_path = &args[1];

    let key_store_dir = fs::read_dir(key_store_path).expect("Read key store directory");

    let entries = key_store_dir
        .map(|res| res.map(|e| e.path()))
        .collect::<io::Result<Vec<_>>>()
        .expect("Read key store entries");

    for entry in entries {
        let data = fs::read(&entry).expect("Read key data");

        let ski = calculate_ski(&data).expect("Calculate SKI");

        println!("{:?} -> {}", &entry, hex::encode(ski));
    }
}
