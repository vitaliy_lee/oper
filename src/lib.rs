use std::fs;
use std::io;
use std::path::Path;
use std::rc::Rc;

use openssl::ec::{EcGroup, EcKey};
use openssl::hash::{hash, MessageDigest};
use openssl::nid::Nid;
use openssl::pkey::{PKey, Private, Public};
use openssl::x509::X509;

use fcrypto::{self, DigestAlgorithm};

use crate::keystore::*;

pub mod keystore;

pub struct CSP<T: KeyStore> {
    key_store: T,
}

impl<T: KeyStore> CSP<T> {
    pub fn new(key_store: T) -> CSP<T> {
        CSP { key_store }
    }
}

impl<T: KeyStore> fcrypto::CSP for CSP<T> {
    fn digest(&self, message: &[u8], algorithm: DigestAlgorithm) -> fcrypto::Result<Vec<u8>> {
        log::debug!("digest({:?}, {:?})", hex::encode(message), algorithm);

        let message_digest = get_message_digest(algorithm);
        let digest = hash(message_digest, message)?;
        Ok(digest.to_vec())
    }

    fn get_digester(
        &self,
        algorithm: DigestAlgorithm,
    ) -> fcrypto::Result<Box<dyn fcrypto::Digester>> {
        log::debug!("get_digester({:?})", algorithm);

        let message_digest = MessageDigest::sha256();
        let digester = Digester::new(message_digest);
        Ok(Box::new(digester))
    }

    fn generate_random(&self, length: u64) -> fcrypto::Result<Vec<u8>> {
        log::debug!("generate_random({:?})", length);

        let mut buffer = vec![0_u8; length as usize];
        openssl::rand::rand_bytes(&mut buffer)?;
        Ok(buffer)
    }

    fn new_signer(
        &self,
        ski: &[u8],
        algorithm: fcrypto::DigestAlgorithm,
    ) -> fcrypto::Result<Box<dyn fcrypto::Signer>> {
        log::debug!("new_signer({:?}, {:?})", hex::encode(ski), algorithm);

        let private_key = self.key_store.get_private_key(ski)?;
        let message_digest = get_message_digest(algorithm);
        let signer = Signer::new(private_key, message_digest);
        Ok(Box::new(signer))
    }

    fn new_verifier(
        &self,
        ski: &[u8],
        algorithm: fcrypto::DigestAlgorithm,
    ) -> fcrypto::Result<Box<dyn fcrypto::Verifier>> {
        log::debug!("new_verifier({:?}, {:?})", hex::encode(ski), algorithm);

        let public_key = self.key_store.get_public_key(ski)?;
        let message_digest = get_message_digest(algorithm);
        let verifier = Verifier::new(public_key, message_digest);
        Ok(Box::new(verifier))
    }
}

pub struct Digester {
    hasher: openssl::hash::Hasher,
}

impl Digester {
    fn new(message_digest: MessageDigest) -> Digester {
        let hasher = openssl::hash::Hasher::new(message_digest).unwrap();

        Digester { hasher }
    }
}

impl fcrypto::Digester for Digester {
    fn init(&mut self) -> fcrypto::Result<()> {
        // self.hasher = openssl::hash::Hasher::new(message_digest)?;
        Ok(())
    }

    fn update(&mut self, data: &[u8]) -> fcrypto::Result<()> {
        self.hasher.update(data).map_err(wrap_error)
    }

    fn digest(&mut self) -> fcrypto::Result<Vec<u8>> {
        let digest = self.hasher.finish()?;
        Ok(digest.to_vec())
    }
}

pub fn wrap_error(error: openssl::error::ErrorStack) -> Box<dyn std::error::Error> {
    Box::new(error)
}

pub struct Signer {
    private_key: Rc<PKey<Private>>,
    message_digest: MessageDigest,
}

impl Signer {
    fn new(private_key: Rc<PKey<Private>>, message_digest: MessageDigest) -> Signer {
        Signer {
            private_key,
            message_digest,
        }
    }
}

impl fcrypto::Signer for Signer {
    fn sign(&self, message: &[u8]) -> fcrypto::Result<Vec<u8>> {
        log::debug!("Signing message: {:?}", hex::encode(message));

        let mut signer = openssl::sign::Signer::new(self.message_digest, &self.private_key)?;
        signer.sign_oneshot_to_vec(message).map_err(wrap_error)
    }
}

pub struct Verifier {
    public_key: Rc<PKey<Public>>,
    message_digest: MessageDigest,
}

impl Verifier {
    fn new(public_key: Rc<PKey<Public>>, message_digest: MessageDigest) -> Verifier {
        Verifier {
            public_key,
            message_digest,
        }
    }
}

impl fcrypto::Verifier for Verifier {
    fn verify(&self, message: &[u8], signature: &[u8]) -> fcrypto::Result<bool> {
        log::debug!("Verifying message: {:?}", hex::encode(message));

        let mut verifier = openssl::sign::Verifier::new(self.message_digest, &self.public_key)?;
        verifier
            .verify_oneshot(signature, message)
            .map_err(wrap_error)
    }
}

#[inline]
fn get_message_digest(algorithm: DigestAlgorithm) -> MessageDigest {
    match algorithm {
        DigestAlgorithm::SHA256 => MessageDigest::sha256(),
        DigestAlgorithm::SHA512 => MessageDigest::sha512(),
    }
}

fn generate_ec_keypair(nid: Nid) -> fcrypto::Result<(PKey<Private>, PKey<Public>)> {
    let group = EcGroup::from_curve_name(nid)?;

    let private_ec_key = EcKey::generate(&group)?;
    let public_ec_key = EcKey::from_public_key(&group, private_ec_key.public_key())?;

    let private_key = PKey::from_ec_key(private_ec_key)?;
    let public_key = PKey::from_ec_key(public_ec_key)?;

    Ok((private_key, public_key))
}

pub fn open_private_key<P: AsRef<Path>>(path: P) -> fcrypto::Result<PKey<Private>> {
    let key_data = fs::read(path)?;
    PKey::private_key_from_pem(&key_data).map_err(wrap_error)
}

pub fn open_certificate<P: AsRef<Path>>(path: P) -> fcrypto::Result<X509> {
    let data = fs::read(path)?;
    X509::from_pem(&data).map_err(wrap_error)
}

pub fn calculate_ski(key_data: &[u8]) -> fcrypto::Result<Vec<u8>> {
    let private_key = PKey::private_key_from_pem(key_data)?;

    let data = private_key.public_key_to_der()?;

    let message_digest = MessageDigest::sha1();
    let digest = hash(message_digest, &data)?;

    return Ok(digest.to_vec());
}

pub fn get_keystore_skis(path: &Path) -> fcrypto::Result<Vec<Vec<u8>>> {
    let mut skis = Vec::new();

    let key_store_dir = fs::read_dir(path)?;

    let entries = key_store_dir
        .map(|res| res.map(|e| e.path()))
        .collect::<io::Result<Vec<_>>>()?;

    for entry in entries {
        let data = fs::read(&entry)?;
        let ski = calculate_ski(&data)?;
        skis.push(ski);
    }

    return Ok(skis);
}

#[cfg(test)]
mod tests {
    use crate::*;

    use fcrypto::{self, Digester, Signer, Verifier};
    use hex;
    use openssl::hash::MessageDigest;
    use std::path::Path;

    #[test]
    fn test_digest() {
        let message = b"Fabrik!Fabrik!";

        let mut digester = crate::Digester::new(MessageDigest::sha256());
        digester.init().expect("Init digester");

        digester.update(message).expect("Update digester");

        let result = digester.digest();
        assert!(result.is_ok());

        let digest = result.unwrap();
        println!("{}", hex::encode(digest));
    }

    #[test]
    fn test_sign_verify() {
        let key_path = "testdata/msp/keystore/peer0.org1.example.com.key.pem";
        let certificate_path = "testdata/msp/signcerts/peer0.org1.example.com.cert.pem";

        let private_key = open_private_key(key_path).expect("Open private key");

        let message = b"Fabrik!Fabrik!";

        let signer = crate::Signer::new(Rc::new(private_key), MessageDigest::sha256());

        let signature = signer.sign(message).expect("Sign message");

        let certificate = open_certificate(certificate_path).expect("Parse certificate");

        let public_key = certificate.public_key().expect("Get public key");

        let verifier = crate::Verifier::new(Rc::new(public_key), MessageDigest::sha256());

        let valid = verifier
            .verify(message, &signature)
            .expect("Verify message");

        assert!(valid);
    }

    #[test]
    fn test_keystore() {
        let ski =
            hex::decode("1194682393f7b46f50a347d8db66565115dcdc48").expect("Read private key file");

        let path = Path::new("testdata/msp/keystore");

        let key_store = FileKeyStore::new(path).expect("Create key store");

        let result = key_store.get_private_key(&ski);
        assert!(result.is_ok());
    }

    #[test]
    fn test_generate_ec_key_pair() {
        let result = generate_ec_keypair(Nid::SECP256K1);
        assert!(result.is_ok());

        let (private_key, public_key) = result.unwrap();

        println!("Size: {:?}", private_key.size());
        println!("ID: {:?}", public_key.id());
    }

    #[test]
    fn test_calculate_ski() {
        let key_data = fs::read("testdata/msp/keystore/peer0.org1.example.com.key.pem")
            .expect("Read private key file");

        let ski = calculate_ski(&key_data).expect("Calculate SKI");

        let expected_ski = hex::decode("1194682393f7b46f50a347d8db66565115dcdc48").unwrap();

        assert_eq!(expected_ski, ski);
    }
}
